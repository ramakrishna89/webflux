package com.org.productservice.service.component;

import com.github.javafaker.Faker;
import com.org.productservice.dto.ProductDto;
import com.org.productservice.service.inter.ProductService;
import com.org.productservice.service.mapper.ProductMapper;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class DataLoaderComponent implements CommandLineRunner {

    private final ProductService productService;
    private final ProductMapper productMapper;
    private final Faker faker;


    @Override
    public void run(String... args) throws Exception {
        Flux.range(1, 10)
                .map(integer -> ProductDto.builder()
                        .description(faker.commerce().productName())
                        .price(Double.parseDouble(faker.commerce().price(10.05, 1000.00)))
                        .build())
                .flatMap(productDto -> productService.insert(Mono.just(productDto)))
                .subscribe(System.out::println);

    }
}
