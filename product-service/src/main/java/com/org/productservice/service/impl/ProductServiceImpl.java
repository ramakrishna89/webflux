package com.org.productservice.service.impl;

import com.org.productservice.dto.ProductDto;
import com.org.productservice.repository.ProductRepository;
import com.org.productservice.service.inter.ProductService;
import com.org.productservice.service.mapper.ProductMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Range;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;


    @Override
    public Flux<ProductDto> getAll() {
        return productRepository.findAll()
                .map(entity -> productMapper.toDto(entity));
    }

    @Override
    public Mono<ProductDto> getById(String id) {
        return productRepository.findById(id)
                .map(entity -> productMapper.toDto(entity));
    }

    @Override
    public Mono<ProductDto> insert(Mono<ProductDto> productDto) {
        return productDto
                .map(dto -> productMapper.toEntity(dto))
                .flatMap(entity -> productRepository.insert(entity))
                .map(entity -> productMapper.toDto(entity));
    }

    @Override
    public Mono<ProductDto> update(String id, Mono<ProductDto> productDto) {
        return productRepository.findById(id)
                .flatMap(entity -> productDto
                        .map(dto -> productMapper.toEntity(dto))
                        .doOnNext(dtoEntity -> dtoEntity.setId(entity.getId())))
                .flatMap(entity -> productRepository.save(entity))
                .map(entity -> productMapper.toDto(entity));

    }

    @Override
    public Mono<Void> delete(String id) {
        return productRepository.deleteById(id);
    }


    @Override
    public Flux<ProductDto> getAllProductByPriceRange(Double min, Double max) {
        return productRepository.findByPriceBetween(Range.closed(min, max))
                .map(entity -> productMapper.toDto(entity));
    }
}
