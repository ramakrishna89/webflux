package com.org.productservice.service.mapper;

import com.org.productservice.dto.ProductDto;
import com.org.productservice.entity.ProductEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    ProductDto toDto(ProductEntity entity);

    ProductEntity toEntity(ProductDto dto);
}
