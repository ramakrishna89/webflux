package com.org.productservice.service.inter;

import com.org.productservice.dto.ProductDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ProductService {

    Flux<ProductDto> getAll();

    Flux<ProductDto> getAllProductByPriceRange(Double min, Double max);

    Mono<ProductDto> getById(String id);

    Mono<ProductDto> insert(Mono<ProductDto> productDto);

    Mono<ProductDto> update(String id, Mono<ProductDto> productDto);

    Mono<Void> delete(String id);
}
