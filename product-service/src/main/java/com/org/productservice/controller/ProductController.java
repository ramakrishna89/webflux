package com.org.productservice.controller;


import com.org.productservice.dto.ProductDto;
import com.org.productservice.service.inter.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@AllArgsConstructor
@RequestMapping("product")
public class ProductController {

    private final ProductService productService;

    @GetMapping
    public Flux<ProductDto> all() {
        return productService.getAll();
    }

    @GetMapping("{id}")
    public Mono<ResponseEntity<ProductDto>> oneById(@PathVariable String id) {
        return productService.getById(id)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping
    public Mono<ProductDto> insert(@RequestBody Mono<ProductDto> dto) {
        return productService.insert(dto);
    }

    @PutMapping("{id}")
    public Mono<ResponseEntity<ProductDto>> insert(@PathVariable String id, @RequestBody Mono<ProductDto> dto) {
        return productService.update(id, dto)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @DeleteMapping("{id}")
    public Mono<Void> delete(@PathVariable String id) {
        return productService.delete(id);
    }

    @GetMapping("by-price")
    public Flux<ProductDto> getByPrice(@RequestParam Double min, @RequestParam Double max) {
        return productService.getAllProductByPriceRange(min, max);
    }


}
