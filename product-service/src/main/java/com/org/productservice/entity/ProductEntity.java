package com.org.productservice.entity;


import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class ProductEntity {

    @Id
    private String id;
    private String description;
    private double price;
}
