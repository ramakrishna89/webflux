package com.org.productservice.repository;

import com.org.productservice.entity.ProductEntity;
import org.springframework.data.domain.Range;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface ProductRepository extends ReactiveMongoRepository<ProductEntity, String> {

    // > min < max (not include = cases)
    // Flux<ProductEntity> findByPriceBetween(Double min, Double max);

    Flux<ProductEntity> findByPriceBetween(Range<Double> range);
}
