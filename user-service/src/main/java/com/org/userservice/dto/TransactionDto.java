package com.org.userservice.dto;


import com.org.userservice.enums.TransactionStatus;
import lombok.Data;

@Data
public class TransactionDto {

    private Integer userId;
    private Double amount;
    private TransactionStatus transactionStatus;

}
