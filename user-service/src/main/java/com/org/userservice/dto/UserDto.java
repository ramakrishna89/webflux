package com.org.userservice.dto;


import lombok.Data;

@Data
public class UserDto {

    private Integer id;
    private String name;
    private Double balance;
}
