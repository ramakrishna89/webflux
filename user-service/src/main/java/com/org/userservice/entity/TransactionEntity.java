package com.org.userservice.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Data
@Table("user_transactions")
public class TransactionEntity {

    @Id
    private Integer id;
    private Integer userId;
    private Double amount;
    private LocalDateTime transDateTime;
}
