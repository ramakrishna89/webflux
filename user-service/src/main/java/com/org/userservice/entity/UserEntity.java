package com.org.userservice.entity;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Table("users")
public class UserEntity {

    @Id
    private Integer id;
    private String name;
    private Double balance;
}
