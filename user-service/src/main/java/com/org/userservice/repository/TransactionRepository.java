package com.org.userservice.repository;

import com.org.userservice.entity.TransactionEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends ReactiveCrudRepository<TransactionEntity, Integer> {
}
