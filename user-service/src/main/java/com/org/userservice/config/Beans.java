package com.org.userservice.config;

import com.github.javafaker.Faker;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Beans {


    @Bean
    public Faker getFaker() {
        return Faker.instance();
    }
}
