package com.org.userservice.controller;


import com.org.userservice.dto.TransactionDto;
import com.org.userservice.service.intr.TransactionService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@AllArgsConstructor
@RequestMapping("transaction")
public class TransactionController {

    private final TransactionService transactionService;


    @PostMapping
    public Mono<TransactionDto> insert(@RequestBody Mono<TransactionDto> dtoMono) {
        return dtoMono
                .flatMap(dto -> transactionService.insert(dto));
    }

}
