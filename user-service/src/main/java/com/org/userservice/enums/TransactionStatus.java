package com.org.userservice.enums;

public enum TransactionStatus {
    APPROVED,
    DECLINED;
}
