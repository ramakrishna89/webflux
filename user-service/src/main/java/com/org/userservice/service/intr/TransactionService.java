package com.org.userservice.service.intr;

import com.org.userservice.dto.TransactionDto;
import reactor.core.publisher.Mono;

public interface TransactionService {

    Mono<TransactionDto> insert(TransactionDto dto);
}
