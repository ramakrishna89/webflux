package com.org.userservice.service.intr;

import com.org.userservice.dto.UserDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserService {

    Flux<UserDto> getAll();

    Mono<UserDto> getById(Integer id);

    Mono<UserDto> insert(Mono<UserDto> userDtoMono);

    Mono<UserDto> update(Integer id, Mono<UserDto> userDtoMono);

    Mono<Void> delete(Integer id);

}
