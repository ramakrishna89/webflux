package com.org.userservice.service.impl;

import com.org.userservice.dto.UserDto;
import com.org.userservice.repository.UserRepository;
import com.org.userservice.service.intr.UserService;
import com.org.userservice.service.mapper.UserMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Override
    public Flux<UserDto> getAll() {
        return userRepository.findAll()
                .map(entity -> userMapper.toDto(entity));
    }

    @Override
    public Mono<UserDto> getById(Integer id) {
        return userRepository.findById(id)
                .map(entity -> userMapper.toDto(entity));
    }

    @Override
    public Mono<UserDto> insert(Mono<UserDto> userDtoMono) {
        return userDtoMono
                .flatMap(dto -> userRepository.save(userMapper.toEntity(dto)))
                .map(entity -> userMapper.toDto(entity));
    }

    @Override
    public Mono<UserDto> update(Integer id, Mono<UserDto> userDtoMono) {
        return userRepository.findById(id)
                .flatMap(entity -> userDtoMono.map(dto -> userMapper.toEntity(dto))
                        .doOnNext(mappedEntity -> mappedEntity.setId(entity.getId())))
                .flatMap(entity -> userRepository.save(entity))
                .map(entity -> userMapper.toDto(entity));
    }

    @Override
    public Mono<Void> delete(Integer id) {
        return userRepository.deleteById(id);
    }
}
