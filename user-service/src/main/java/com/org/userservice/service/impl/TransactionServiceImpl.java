package com.org.userservice.service.impl;


import com.org.userservice.dto.TransactionDto;
import com.org.userservice.enums.TransactionStatus;
import com.org.userservice.repository.TransactionRepository;
import com.org.userservice.repository.UserRepository;
import com.org.userservice.service.intr.TransactionService;
import com.org.userservice.service.mapper.TransactionMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

@Service
@AllArgsConstructor
public class TransactionServiceImpl implements TransactionService {

    private final UserRepository userRepository;
    private final TransactionRepository transactionRepository;
    private final TransactionMapper transactionMapper;


    @Override
    public Mono<TransactionDto> insert(TransactionDto dto) {
        return userRepository.updateUserBalance(dto.getUserId(), dto.getAmount())
                .filter(count -> count > 0)
                .map(count -> transactionMapper.toEntity(dto, LocalDateTime.now()))
                .flatMap(entity -> transactionRepository.save(entity))
                .map(entity -> transactionMapper.toDto(entity, TransactionStatus.APPROVED))
                .defaultIfEmpty(transactionMapper.toDto(dto, TransactionStatus.DECLINED));
    }
}
