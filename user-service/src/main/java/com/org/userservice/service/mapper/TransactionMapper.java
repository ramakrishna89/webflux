package com.org.userservice.service.mapper;


import com.org.userservice.dto.TransactionDto;
import com.org.userservice.entity.TransactionEntity;
import com.org.userservice.enums.TransactionStatus;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.LocalDateTime;


@Mapper(componentModel = "spring")
public interface TransactionMapper {

    @Mapping(target = "transDateTime", source = "localDateTime")
    TransactionEntity toEntity(TransactionDto dto, LocalDateTime localDateTime);

    @Mapping(target = "transactionStatus", source = "status")
    TransactionDto toDto(TransactionEntity entity, TransactionStatus status);

    @Mapping(target = "transactionStatus", source = "status")
    TransactionDto toDto(TransactionDto dto, TransactionStatus status);

}
