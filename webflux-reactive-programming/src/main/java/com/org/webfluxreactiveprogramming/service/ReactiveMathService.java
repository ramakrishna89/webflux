package com.org.webfluxreactiveprogramming.service;

import com.org.webfluxreactiveprogramming.dto.MultiplyRequestDto;
import com.org.webfluxreactiveprogramming.dto.Response;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@Service
public class ReactiveMathService {

    public Mono<Response> findSquare(int input) {
        return Mono.fromSupplier(() -> input * input)
                .map(Response::new);
    }

    public Flux<Response> multiTable(int input) {
        return Flux.range(1, 10)
                .delayElements(Duration.ofSeconds(1))
                .doOnNext(i -> System.out.println("MathService processing: " + i))
                .map(i -> new Response(i * input));
    }

    public Mono<Response> multiplyNumbers(Mono<MultiplyRequestDto> dtoMono) {
        return dtoMono
                .map(multiplyRequestDto -> multiplyRequestDto.getFirst() * multiplyRequestDto.getSecond())
                .map(Response::new);
    }
}
