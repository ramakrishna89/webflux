package com.org.webfluxreactiveprogramming.service;

import com.org.webfluxreactiveprogramming.dto.Response;
import com.org.webfluxreactiveprogramming.util.StaticUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class MathService {

    public Response findSquare(int input){
        return new Response(input* input);
    }

    public List<Response> multiTable(int input){
        return IntStream.rangeClosed(1, 10)
                .peek(i -> StaticUtils.sleep(1))
                .peek(i -> System.out.println("MathService processing: " + i))
                .mapToObj(i -> new Response(i * input))
                .collect(Collectors.toList());
    }
}
