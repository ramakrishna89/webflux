package com.org.webfluxreactiveprogramming.routehandler;


import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.function.BiFunction;

@Component
@AllArgsConstructor
public class CalculatorHandler {


    public Mono<ServerResponse> add(ServerRequest request) {
        return process(request, (a, b) -> ServerResponse.ok().bodyValue(a + b));
    }

    public Mono<ServerResponse> subtract(ServerRequest request) {
        return process(request, (a, b) -> ServerResponse.ok().bodyValue(a - b));
    }

    public Mono<ServerResponse> multiply(ServerRequest request) {
        return process(request, (a, b) -> ServerResponse.ok().bodyValue(a * b));
    }

    public Mono<ServerResponse> divide(ServerRequest request) {
        return process(request, (a, b) -> b != 0 ? ServerResponse.ok().bodyValue(a / b) :
                ServerResponse.badRequest().bodyValue("B should not be 0"));
    }

    private Mono<ServerResponse> process(ServerRequest request, BiFunction<Integer, Integer, Mono<ServerResponse>> arithmetic) {
        int a = getValue(request, "a");
        int b = getValue(request, "b");
        return arithmetic.apply(a, b);
    }

    private int getValue(ServerRequest request, String key) {
        return Integer.parseInt(request.pathVariable(key));
    }
}
