package com.org.webfluxreactiveprogramming.routehandler;

import com.org.webfluxreactiveprogramming.dto.MultiplyRequestDto;
import com.org.webfluxreactiveprogramming.dto.Response;
import com.org.webfluxreactiveprogramming.exception.error.InputValidationException;
import com.org.webfluxreactiveprogramming.service.ReactiveMathService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class ReactiveMathValidationRouteHandler {

    private ReactiveMathService service;

    public Mono<ServerResponse> findSquare(ServerRequest request) {
        Integer input = Integer.parseInt(request.pathVariable("input"));
        Mono<Response> mono = service.findSquare(input);
        return ServerResponse.ok().body(mono, Response.class);
    }

    public Mono<ServerResponse> tableHandler(ServerRequest request) {
        Integer input = Integer.parseInt(request.pathVariable("input"));
        Flux<Response> flux = service.multiTable(input);
        return ServerResponse.ok().body(flux, Response.class);
    }

    public Mono<ServerResponse> tableStreamHandler(ServerRequest request) {
        Integer input = Integer.parseInt(request.pathVariable("input"));
        Flux<Response> flux = service.multiTable(input);
        return ServerResponse.ok()
                .contentType(MediaType.TEXT_EVENT_STREAM)
                .body(flux, Response.class);
    }

    public Mono<ServerResponse> multiplyHandler(ServerRequest request) {
        Mono<MultiplyRequestDto> dtoMono = request.bodyToMono(MultiplyRequestDto.class);
        Mono<Response> mono = service.multiplyNumbers(dtoMono);
        return ServerResponse.ok()
                .body(mono, Response.class);
    }

    public Mono<ServerResponse> findSquareWithValidation(ServerRequest request) {
        Integer input = Integer.parseInt(request.pathVariable("input"));
        if (input < 10 || input > 20) {
            // return ServerResponse.badRequest().bodyValue(new InputValidationException());
            // or
            return Mono.error(new InputValidationException());
        }
        Mono<Response> mono = service.findSquare(input);
        return ServerResponse.ok().body(mono, Response.class);
    }

}
