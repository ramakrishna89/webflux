package com.org.webfluxreactiveprogramming.dto;


import lombok.Data;

@Data
public class MultiplyRequestDto {

    private Integer first;
    private Integer second;
}
