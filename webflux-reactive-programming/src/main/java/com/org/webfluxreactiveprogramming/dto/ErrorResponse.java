package com.org.webfluxreactiveprogramming.dto;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorResponse {

    private Integer errorCode;
    private String errorDescription;

}
