package com.org.webfluxreactiveprogramming.controller;

import com.org.webfluxreactiveprogramming.dto.MultiplyRequestDto;
import com.org.webfluxreactiveprogramming.dto.Response;
import com.org.webfluxreactiveprogramming.service.ReactiveMathService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;


@RestController
@RequestMapping("reactive-math")
@AllArgsConstructor
public class ReactiveMathController {

    private ReactiveMathService reactiveMathService;

    @GetMapping("square/{input}")
    public Mono<Response> findSquare(@PathVariable int input) {
        return reactiveMathService.findSquare(input);
    }

    @GetMapping("table/{input}")
    public Flux<Response> findMultiplicationTable(@PathVariable int input) {
        return reactiveMathService.multiTable(input);
    }

    @GetMapping(value = "table/{input}/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Response> findMultiplicationTableStream(@PathVariable int input) {
        return reactiveMathService.multiTable(input);
    }

    @PostMapping("multiply-numbers")
    public Mono<Response> multiplyNumbers(@RequestBody Mono<MultiplyRequestDto> dtoMono, @RequestHeader Map<String, String> headers) {
        System.out.println(headers);
        return reactiveMathService.multiplyNumbers(dtoMono);
    }

}
