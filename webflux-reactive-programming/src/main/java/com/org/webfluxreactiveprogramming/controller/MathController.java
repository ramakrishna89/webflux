package com.org.webfluxreactiveprogramming.controller;

import com.org.webfluxreactiveprogramming.dto.Response;
import com.org.webfluxreactiveprogramming.service.MathService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("math")
@AllArgsConstructor
public class MathController {

    private MathService mathService;

    @GetMapping("square/{input}")
    public Response findSquare(@PathVariable int input){
        return mathService.findSquare(input);
    }

    @GetMapping("table/{input}")
    public List<Response> findMultiplicationTable(@PathVariable int input){
        return mathService.multiTable(input);
    }

}
