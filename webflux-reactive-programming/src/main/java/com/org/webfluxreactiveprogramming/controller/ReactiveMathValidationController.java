package com.org.webfluxreactiveprogramming.controller;

import com.org.webfluxreactiveprogramming.dto.Response;
import com.org.webfluxreactiveprogramming.exception.error.InputValidationException;
import com.org.webfluxreactiveprogramming.service.ReactiveMathService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;


@RestController
@AllArgsConstructor
@RequestMapping("reactive-math-validation")
public class ReactiveMathValidationController {

    private ReactiveMathService reactiveMathService;

    @GetMapping("find-square/{input}")
    public Mono<Response> findSquare(@PathVariable int input) {
        if (input < 10 || input > 20) {
            throw new InputValidationException();
        }
        return reactiveMathService.findSquare(input);
    }

    @GetMapping("find-mono-square/{input}")
    public Mono<Response> findMonoSquare(@PathVariable int input) {
        System.out.println("Inside findMonoSquare");
        return Mono.just(input)
                .handle((integer, synchronousSink) -> {
                    if (integer >= 10 && integer < 20)
                        synchronousSink.next(integer);
                    else {
                        synchronousSink.error(new InputValidationException());
                        // synchronousSink.error(new ResponseStatusException(HttpStatus.BAD_REQUEST, "BAD REQ"));
                    }
                })
                .cast(Integer.class)
                .flatMap(integer -> reactiveMathService.findSquare(integer));
    }

    @GetMapping("find-mono-square/{input}/assignment")
    public Mono<ResponseEntity<Response>> findMonoSquareAssignment(@PathVariable int input) {
        return Mono.just(input)
                .filter(i -> i >= 10 && i < 20)
                .flatMap(i -> reactiveMathService.findSquare(i))
                .map(obj -> ResponseEntity.ok(obj))
                .defaultIfEmpty(ResponseEntity.badRequest().build());

    }


}
