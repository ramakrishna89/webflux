package com.org.webfluxreactiveprogramming.config;

import com.org.webfluxreactiveprogramming.dto.ErrorResponse;
import com.org.webfluxreactiveprogramming.exception.error.InputValidationException;
import com.org.webfluxreactiveprogramming.routehandler.CalculatorHandler;
import com.org.webfluxreactiveprogramming.routehandler.ReactiveMathValidationRouteHandler;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.*;
import reactor.core.publisher.Mono;

import java.util.function.BiFunction;

@Configuration
@AllArgsConstructor
public class RouterConfig {

    private final ReactiveMathValidationRouteHandler handler;
    private final CalculatorHandler calculatorHandler;

    @Bean
    public RouterFunction<ServerResponse> pathServerResponseRouterFunction() {
        return RouterFunctions.route()
                .path("router-reactive-math-validation1", this::serverResponseRouterFunction1)
                .path("calculator", this::calculatorRoute)
                .build();
    }

    private RouterFunction<ServerResponse> calculatorRoute() {

        return RouterFunctions.route()
                .GET("{a}/{b}", checkCalculateURLReq("A"), calculatorHandler::add)
                .GET("{a}/{b}", checkCalculateURLReq("S"), calculatorHandler::subtract)
                .GET("{a}/{b}", checkCalculateURLReq("M"), calculatorHandler::multiply)
                .GET("{a}/{b}", checkCalculateURLReq("D"), calculatorHandler::divide)
                .GET("{a}/{b}", serverRequest -> ServerResponse.badRequest().bodyValue("OP Not Valid"))
                .build();
    }

    private RequestPredicate checkCalculateURLReq(String operation) {
        return RequestPredicates.headers(headers -> operation.equals(
                headers.asHttpHeaders().toSingleValueMap().get("OP")));
    }

    //@Bean //if path function then bean annotation is not required, we can use multiple router functions
    public RouterFunction<ServerResponse> serverResponseRouterFunction1() {
        return RouterFunctions.route()
                .GET("find-square/{input}", handler::findSquare)
                .GET("find-table/{input}", handler::tableHandler)
                .GET("table-stream/{input}", handler::tableStreamHandler)
                .POST("multiply", handler::multiplyHandler)
                .GET("find-square-validation/{input}", handler::findSquareWithValidation)
                .onError(InputValidationException.class, exceptionHandler())
                .build();
    }

    @Bean
    public RouterFunction<ServerResponse> serverResponseRouterFunction() {
        return RouterFunctions.route()
                .GET("router-reactive-math-validation/find-square/{input}", handler::findSquare)
                .GET("router-reactive-math-validation/find-table/{input}", handler::tableHandler)
                .GET("router-reactive-math-validation/table-stream/{input}", handler::tableStreamHandler)
                .POST("router-reactive-math-validation/multiply", handler::multiplyHandler)
                .GET("router-reactive-math-validation/find-square-validation/{input}", handler::findSquareWithValidation)
                .onError(InputValidationException.class, exceptionHandler())
                .build();
    }


    private BiFunction<Throwable, ServerRequest, Mono<ServerResponse>> exceptionHandler() {
        return (throwable, request) -> {
            return ServerResponse.badRequest().bodyValue(new ErrorResponse(100, "Input Validation Exception"));
        };
    }
}
