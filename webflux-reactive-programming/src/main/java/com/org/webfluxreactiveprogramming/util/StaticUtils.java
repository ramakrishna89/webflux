package com.org.webfluxreactiveprogramming.util;

public class StaticUtils {

    public static void sleep(int seconds){
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
