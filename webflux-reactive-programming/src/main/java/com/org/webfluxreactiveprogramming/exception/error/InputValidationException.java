package com.org.webfluxreactiveprogramming.exception.error;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InputValidationException extends RuntimeException {
}
