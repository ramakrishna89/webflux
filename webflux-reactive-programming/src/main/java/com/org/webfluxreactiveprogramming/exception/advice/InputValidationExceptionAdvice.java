package com.org.webfluxreactiveprogramming.exception.advice;

import com.org.webfluxreactiveprogramming.dto.ErrorResponse;
import com.org.webfluxreactiveprogramming.exception.error.InputValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class InputValidationExceptionAdvice {

    @ExceptionHandler(InputValidationException.class)
    public ResponseEntity<ErrorResponse> handleInputValidationException(InputValidationException ex) {
        return ResponseEntity.badRequest().body(new ErrorResponse(100, "Invalid input"));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleException(Exception ex) {
        return ResponseEntity.badRequest().body(new ErrorResponse(1, ex.getMessage()));
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ErrorResponse> handleRuntimeException(RuntimeException ex) {
        return ResponseEntity.badRequest().body(new ErrorResponse(2, ex.getMessage()));
    }
}
