package com.org.webfluxreactiveprogramming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebfluxReactiveProgrammingApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebfluxReactiveProgrammingApplication.class, args);
    }

}
