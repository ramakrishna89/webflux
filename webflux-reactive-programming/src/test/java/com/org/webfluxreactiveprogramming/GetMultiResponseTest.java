package com.org.webfluxreactiveprogramming;


import com.org.webfluxreactiveprogramming.dto.Response;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class GetMultiResponseTest extends BaseTest {


    @Test
    public void stepVerifierFluxTest() {
        Flux<Response> response = this.webClient
                .get()
                .uri("reactive-math/table/{in}", 5)
                .retrieve()
                .bodyToFlux(Response.class)
                .doOnNext(System.out::println);

        StepVerifier.create(response)
                .expectNextCount(10)
                .verifyComplete();
    }

    @Test
    public void stepVerifierFluxStreamTest() {
        Flux<Response> response = this.webClient
                .get()
                .uri("reactive-math/table/{input}/stream", 5)
                .retrieve()
                .bodyToFlux(Response.class)
                .doOnNext(System.out::println);

        StepVerifier.create(response)
                .expectNextCount(10)
                .verifyComplete();
    }

}
