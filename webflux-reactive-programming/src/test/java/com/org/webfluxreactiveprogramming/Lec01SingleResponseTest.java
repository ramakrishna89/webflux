package com.org.webfluxreactiveprogramming;

import com.org.webfluxreactiveprogramming.dto.Response;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.Duration;

public class Lec01SingleResponseTest extends BaseTest {


    @Test
    public void blockTestSquare() {
        this.webClient
                .get()
                .uri("reactive-math/square/{in1}", 10)
                .retrieve()
                .bodyToMono(Response.class)
                .block(Duration.ofSeconds(10));
    }


    @Test
    public void blockTest() {
        this.webClient
                .get()
                .uri("calculator/{in1}/{in2}", 10, 2)
                .header("OP", "A")
                .retrieve()
                .bodyToMono(Integer.class)
                .block();
    }


    @Test
    public void stepVerifierTest() {
        Mono<Response> response = this.webClient
                .get()
                .uri("reactive-math/square/{in1}", 5)
                .header("OP", "A")
                .retrieve()
                .bodyToMono(Response.class);

        StepVerifier.create(response)
                .expectNextMatches(r -> r.getOutput() == 25)
                .verifyComplete();
    }


}
